﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraitementDonnees__Data_Process_
{
    public class StringProcess
    {
        public static Tuile[,] String2TuileArray(string entry)
        {
            string[] lignes = entry.Split('|');
            if (lignes.Length != 11)
                throw new TaskCanceledException("La chaine de caractères ne permet pas d'avoir 10 lignes pour le tableau.");
            Tuile[,] map = new Tuile[10, 10];
            for (int i = 0; i < 10; i++)
            {
                string[] line = lignes[i].Split(':');
                if (line.Length != 10)
                    throw new TaskCanceledException("La chaine de caractères ne permet pas d'avoir 10 colonnes pour chaque lignes.\nVoir ligne n°" + i);
                for (int j = 0; j < 10; j++)
                {
                    map[i, j] = new Tuile(line[j]);
                }
            }
            return map;
        }
    }
}
