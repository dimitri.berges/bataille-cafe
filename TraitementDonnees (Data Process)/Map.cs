﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraitementDonnees__Data_Process_
{
    public class Map
    {
        /*** Propriétés ***/
        public Tuile[,] Plateau { get; private set; }
        public int[,] IdsParcelles { get; set; }
        public Parcelle[] Parcelles { get; private set; }
        public int NbrGraines { get; private set; }

        /*** Constructeur ***/
        public Map()
        {
            Plateau = new Tuile[10, 10];
            IdsParcelles = new int[10, 10];
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    Plateau[i, j] = new Tuile();
                    IdsParcelles[i, j] = -1;
                }
            Parcelles = new Parcelle[17];
            for (int i = 0; i < Parcelles.Length; i++)
            {
                Parcelles[i] = new Parcelle(this, i);
            }
            NbrGraines = 0;
        }
        public Map(string entry)
        {
            Plateau = StringProcess.String2TuileArray(entry);
            IdsParcelles = new int[10, 10];
            for (int i = 0; i < 10; i++) for (int j = 0; j < 10; j++) IdsParcelles[i, j] = -1;
            Parcelles = new Parcelle[17];
            for (int i = 0; i < Parcelles.Length; i++)
            {
                Parcelles[i] = new Parcelle(this, i);
            }
            NbrGraines = 0;
        }

        /*** Méthodes ***/
        public void changeMap(string entry)
        {
            this.Plateau = StringProcess.String2TuileArray(entry);
        }
        public Tuile Tuile(int i, int j)
        {
            return Plateau[i, j];
        }

        public bool PlaceGraine(int x, int y, int joueur)
        {
            if (Plateau[x, y].Graine == 0)
            {
                Plateau[x, y].Graine = joueur;
                NbrGraines++;
                Parcelles[IdsParcelles[x, y]].CheckOwner();
                return true;
            } else
            {
                return false;
            }
        }
    }
}
